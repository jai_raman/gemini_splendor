"""
from .models import *

import json
from django.http import HttpResponse,HttpResponseServerError

def get_apartments(request):
    all_apts=apartment.objects.all().values('id','apartment_no','apt_flat_type','apt_foor_no','apt_view_name','is_available','cost_per_sqft','apt_building','actual_area_suite','actual_area_balcony',)
    return HttpResponse(json.dumps(list(all_apts)),content_type = 'application/javascript; charset=utf8')



def get_type_unit(self,request):
    type1_aptments=apartment.objects.filter(apt_foor_no=request.data['apt_foor_no'],apt_flat_type=request.data['apt_flat_type'])

    return HttpResponse(json.dumps(list(type1_aptments)),content_type = 'application/javascript; charset=utf8')

# import json
# def user_data(request):
#  if request.POST(): #os request.GET()
#     get_value= request.body
#     # Do your logic here coz you got data in `get_value`
#
#     data = {}
#     data['result'] = 'you made a request'
#     return HttpResponse(json.dumps(data), content_type="application/json")

def save_user_data(request):
    if request.method == 'POST':
        json_data = json.loads(request.body) # request.raw_post_data w/ Django < 1.4
        try:
          # name = json_data['name']
          # email = json_data['email']
          # option_selected = json_data['option_selected']
          user_ob = Userdata(user_name=json_data['name'],user_email=json_data['email'],user_mobile=json_data['mobile'],intrested_in_apt=json_data['apartment'])
          user_ob.save()


        except KeyError:
          return HttpResponseServerError('mal data')

    if request.method == 'GET':
        apartment_id = request.GET['apartment_id']
        optionValue = request.GET['option']

    return HttpResponse("Got json data")


"""

from app1.models import *

from .serializers import *
from rest_framework import response,status
from rest_framework.viewsets import ModelViewSet


class apartment_1_viewset(ModelViewSet):
    queryset = apartment.objects.all()
    serializer_class = apartment_serializer

    # def list(self, request, *args, **kwargs):
    #     flat_type = request.GET.get('flat_type')
    #     try:
    #         queryset = apartment.objects.filter(apt_flat_type__flat_type_name=flat_type,is_available=True)
    #         serializer = apartment_serializer(instance=queryset, many=True)
    #         # qs = self.filter_queryset(queryset)
    #         # page = self.paginate_queryset(qs)
    #         # if page is not None:
    #         #     serializer = apartment_serializer(page, many=True)
    #
    #         for temp in serializer.data:
    #             temp['total_area']= temp['actual_area_suite'] + temp['actual_area_balcony']
    #
    #         return response.Response(serializer.data)
    #     except Exception as e:
    #         return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)


class customer_data(ModelViewSet):
    queryset = Userdata.objects.all()
    serializer_class = user_data_serializer
    # def create(self, request, *args, **kwargs):
    #
    #     user_ob = Userdata()
    #
    #     if 'user_name' in request.data:
    #         user_ob.user_name = request.data['user_name']
    #     if 'user_email' in request.data:
    #         user_ob.user_email = request.data['user_email']
    #     if 'user_mobile' in request.data:
    #         user_ob.user_email = request.data['user_email']
    #     if 'payment_plan' in request.data:
    #         user_ob.payment_plan = request.data['payment_plan']


class apartment_viewset(ModelViewSet):
    queryset = apartment.objects.all()
    serializer_class = apartment_serializer


    def create(self, request, *args, **kwargs):
        try:

            if 'option' in request.data:
                option = int(request.data['option'])
                print option

            if 'flat_type' in request.data:
                flat_data = request.data['flat_type']
                print flat_data

                queryset = apartment.objects.filter(apt_flat_type__flat_type_name=flat_data)
                serializer = apartment_serializer(instance=queryset, many=True)
                #serializer.data[0]['total_area']= serializer.data[0]['actual_area_suite']+serializer.data[0]['actual_area_balcony']

                if option == 1:
                    for temp in serializer.data:
                        temp['option_description'] = '25% DURING CONSTRUCTION, 75% ON COMPLETION'

                        temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']

                        temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                    return response.Response(serializer.data)
                if option == 2:
                    for temp in serializer.data:
                        temp['option_description'] = '45% DURING CONSTRUCTION, 15% ON COMPLETION, 40% IN 2 YEARS POST COMPLETION ( 8 equal cheques of 3 months gap)'
                        temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                        temp['cost_per_sqft'] += 35
                        temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                    return response.Response(serializer.data)
                if option == 3:
                    for temp in serializer.data:
                        temp['option_description'] = '35% DURING CONSTRUCTION, 15% ON COMPLETION, 50% IN 2 YEARS POST COMPLETION( 8 equal cheques of 3 months gap)'
                        temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                        temp['cost_per_sqft'] += 40
                        temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                    return response.Response(serializer.data)
                if option == 4:
                    for temp in serializer.data:
                        temp['option_description'] = '45% DURING CONSTRUCTION, 15% ON COMPLETION, 40% IN 3 YEARS POST COMPLETION (12 equal cheques of 3 months gap)'
                        temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                        temp['cost_per_sqft'] += 55
                        temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                    return response.Response(serializer.data)
                if option == 5:
                    for temp in serializer.data:
                        temp['option_description'] = '35% DURING CONSTRUCTION, 15% ON COMPLETION, 50% IN 3 YEARS POST COMPLETION (12 equal cheques of 3 months gap)'
                        temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                        temp['cost_per_sqft'] += 70
                        temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                    return response.Response(serializer.data)
                if option == 6:
                    for temp in serializer.data:
                        temp['option_description'] = '45% DURING CONSTRUCTION, 15% ON COMPLETION, 40% IN 5 YEARS POST COMPLETION (20 equal cheques of 3 months gap)'
                        temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                        temp['cost_per_sqft'] += 80
                        temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                    return response.Response(serializer.data)
                if option == 7:
                    for temp in serializer.data:
                        temp['option_description'] = '35% DURING CONSTRUCTION, 15% ON COMPLETION, 50% IN 5 YEARS POST COMPLETION (20 equal cheques of 3 months gap)'
                        temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                        temp['cost_per_sqft'] += 100
                        temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                    return response.Response(serializer.data)

            return response.Response(serializer.data)
        except Exception as e:
            return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)



    def list(self, request, *args, **kwargs):
        flat_type = request.GET.get('flat_type')
        option = int(request.GET.get('option'))



        try:
            queryset = apartment.objects.filter(apt_flat_type__flat_type_name=flat_type)
            serializer = apartment_serializer(instance=queryset, many=True)
            # qs = self.filter_queryset(queryset)
            # page = self.paginate_queryset(qs)
            # if page is not None:
            #     serializer = apartment_serializer(page, many=True)

            if option == 1:
                for temp in serializer.data:
                    temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']

                    temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                return response.Response(serializer.data)
            if option == 2:
                for temp in serializer.data:
                    temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                    temp['cost_per_sqft'] += 35
                    temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                return response.Response(serializer.data)
            if option == 3:
                for temp in serializer.data:
                    temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                    temp['cost_per_sqft'] += 40
                    temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                return response.Response(serializer.data)
            if option == 4:
                for temp in serializer.data:
                    temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                    temp['cost_per_sqft'] += 55
                    temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                return response.Response(serializer.data)
            if option == 5:
                for temp in serializer.data:
                    temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                    temp['cost_per_sqft'] += 70
                    temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                return response.Response(serializer.data)
            if option == 6:
                for temp in serializer.data:
                    temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                    temp['cost_per_sqft'] += 80
                    temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                return response.Response(serializer.data)
            if option == 7:
                for temp in serializer.data:
                    temp['total_area'] = temp['actual_area_suite'] + temp['actual_area_balcony']
                    temp['cost_per_sqft'] += 100
                    temp['total_price'] = temp['total_area']*temp['cost_per_sqft']
                return response.Response(serializer.data)

            return response.Response(serializer.data)
        except Exception as e:
            return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)


class create_test(ModelViewSet):
    pass

