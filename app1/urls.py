from django.conf.urls import include, url
from django.contrib import admin
from . import views

from rest_framework.routers import DefaultRouter,SimpleRouter

from .views import *

router = SimpleRouter()


router.register(r'apartments_options',apartment_viewset, 'apartments_options')
router.register(r'apartments',apartment_1_viewset, 'apartments')
router.register(r'customer_data',customer_data, 'customer_data')

urlpatterns = router.urls
