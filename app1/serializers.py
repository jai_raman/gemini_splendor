from .models import *
from rest_framework.serializers import ModelSerializer


class apartment_serializer(ModelSerializer):
    class Meta:
        model = apartment
        fields = '__all__'

class floor_serializer(ModelSerializer):
    class Meta:
        model = Floor_no
        fields = '__all__'

class building_serializer(ModelSerializer):
    class Meta:
        model = building
        fields = '__all__'

class apt_view_serializer(ModelSerializer):
    class Meta:
        model = apt_view
        fields = '__all__'

class user_data_serializer(ModelSerializer):
    class Meta:
        model = Userdata
        fields = '__all__'