# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='apartment',
            name='total_area_sqft',
        ),
        migrations.AlterField(
            model_name='apartment',
            name='actual_area_balcony',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='apartment',
            name='actual_area_suite',
            field=models.FloatField(),
        ),
    ]
