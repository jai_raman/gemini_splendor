# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0004_options_option_description'),
    ]

    operations = [
        migrations.RenameField(
            model_name='apartment',
            old_name='option1_persqft',
            new_name='cost_per_sqft',
        ),
    ]
