# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0015_auto_20170901_0902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userdata',
            name='apt_name',
            field=models.ForeignKey(to='app1.apartment'),
        ),
    ]
