# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0019_auto_20170901_1109'),
    ]

    operations = [
        migrations.AddField(
            model_name='apartment',
            name='total_area',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='apartment',
            name='total_price',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
