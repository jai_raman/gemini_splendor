# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0013_auto_20170901_0901'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userdata',
            name='unit_size',
        ),
        migrations.RemoveField(
            model_name='userdata',
            name='unit_type',
        ),
    ]
