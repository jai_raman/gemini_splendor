# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0012_remove_userdata_intrested_in_apt'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdata',
            name='apt_name',
            field=models.CharField(default='K1', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userdata',
            name='payment_plan',
            field=models.NullBooleanField(default=True),
        ),
        migrations.AddField(
            model_name='userdata',
            name='percentage_of_pay',
            field=models.IntegerField(default=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userdata',
            name='post_handover_plan',
            field=models.NullBooleanField(default=True),
        ),
        migrations.AddField(
            model_name='userdata',
            name='unit_size',
            field=models.IntegerField(default=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userdata',
            name='unit_type',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userdata',
            name='years_of_pay',
            field=models.IntegerField(default=2),
            preserve_default=False,
        ),
    ]
