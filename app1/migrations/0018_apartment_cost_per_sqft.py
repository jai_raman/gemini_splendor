# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0017_remove_apartment_cost_per_sqft'),
    ]

    operations = [
        migrations.AddField(
            model_name='apartment',
            name='cost_per_sqft',
            field=models.BigIntegerField(default=123),
            preserve_default=False,
        ),
    ]
