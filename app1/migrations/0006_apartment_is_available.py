# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0005_auto_20170826_0903'),
    ]

    operations = [
        migrations.AddField(
            model_name='apartment',
            name='is_available',
            field=models.NullBooleanField(default=True),
        ),
    ]
