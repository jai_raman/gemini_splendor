# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0016_auto_20170901_1011'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='apartment',
            name='cost_per_sqft',
        ),
    ]
