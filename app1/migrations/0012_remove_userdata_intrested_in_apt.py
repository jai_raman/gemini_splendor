# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0011_userdata_intrested_in_apt'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userdata',
            name='intrested_in_apt',
        ),
    ]
