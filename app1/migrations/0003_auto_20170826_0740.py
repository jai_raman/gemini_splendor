# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0002_auto_20170825_1930'),
    ]

    operations = [
        migrations.CreateModel(
            name='options',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('option_type', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='apartment',
            name='Option_selected',
            field=models.ForeignKey(default=1, to='app1.options'),
            preserve_default=False,
        ),
    ]
