# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0009_userdata_user_mobile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userdata',
            name='option_selected',
        ),
    ]
