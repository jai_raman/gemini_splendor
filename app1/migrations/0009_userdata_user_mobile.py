# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0008_userdata'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdata',
            name='user_mobile',
            field=models.IntegerField(default=123),
            preserve_default=False,
        ),
    ]
