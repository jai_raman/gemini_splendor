# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0014_auto_20170901_0901'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdata',
            name='unit_size',
            field=models.CharField(default='1b', max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userdata',
            name='unit_type',
            field=models.CharField(default='po', max_length=20),
            preserve_default=False,
        ),
    ]
