# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='apartment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('apartment_no', models.CharField(unique=True, max_length=100)),
                ('actual_area_suite', models.BigIntegerField()),
                ('actual_area_balcony', models.BigIntegerField()),
                ('total_area_sqft', models.BigIntegerField()),
                ('option1_persqft', models.BigIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='apt_view',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('apt_view_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='building',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('building_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Flat_type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('flat_type_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Floor_no',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('floor_no', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='apartment',
            name='apt_building',
            field=models.ForeignKey(to='app1.building'),
        ),
        migrations.AddField(
            model_name='apartment',
            name='apt_flat_type',
            field=models.ForeignKey(blank=True, to='app1.Flat_type', null=True),
        ),
        migrations.AddField(
            model_name='apartment',
            name='apt_foor_no',
            field=models.ForeignKey(to='app1.Floor_no'),
        ),
        migrations.AddField(
            model_name='apartment',
            name='apt_view_name',
            field=models.ForeignKey(to='app1.apt_view'),
        ),
    ]
