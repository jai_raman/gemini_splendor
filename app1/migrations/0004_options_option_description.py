# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0003_auto_20170826_0740'),
    ]

    operations = [
        migrations.AddField(
            model_name='options',
            name='option_description',
            field=models.TextField(default='this is option1'),
            preserve_default=False,
        ),
    ]
