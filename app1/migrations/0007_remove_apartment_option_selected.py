# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0006_apartment_is_available'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='apartment',
            name='Option_selected',
        ),
    ]
