# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0020_auto_20170901_1159'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='apartment',
            name='total_area',
        ),
        migrations.RemoveField(
            model_name='apartment',
            name='total_price',
        ),
    ]
