# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0018_apartment_cost_per_sqft'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userdata',
            name='apt_name',
            field=models.ForeignKey(blank=True, to='app1.apartment', null=True),
        ),
        migrations.AlterField(
            model_name='userdata',
            name='percentage_of_pay',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userdata',
            name='unit_size',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userdata',
            name='unit_type',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userdata',
            name='user_email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userdata',
            name='user_mobile',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userdata',
            name='years_of_pay',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
