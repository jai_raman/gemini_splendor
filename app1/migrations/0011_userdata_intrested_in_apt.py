# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0010_remove_userdata_option_selected'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdata',
            name='intrested_in_apt',
            field=models.ForeignKey(default=1, to='app1.apartment'),
            preserve_default=False,
        ),
    ]
