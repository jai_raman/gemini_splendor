from django.db import models

class Flat_type(models.Model):
    flat_type_name=models.CharField(max_length=100)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.flat_type_name)

class Floor_no(models.Model):
    floor_no=models.CharField(max_length=100)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.floor_no)

class apt_view(models.Model):
    apt_view_name=models.CharField(max_length=100)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.apt_view_name)

class building(models.Model):
    building_name=models.CharField(max_length=100)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.building_name)

class options(models.Model):
    option_type=models.CharField(max_length=100)
    option_description=models.TextField()


    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.option_type)


# class options2(models.Model):
#     per_sq_ft=models.BigIntegerField()
#     Total_price = models.BigIntegerField()
#
# class options3(models.Model):
#     per_sq_ft=models.BigIntegerField()
#     Total_price = models.BigIntegerField()
#
# class options4(models.Model):
#     per_sq_ft=models.BigIntegerField()
#     Total_price = models.BigIntegerField()
#
# class options5(models.Model):
#     per_sq_ft=models.BigIntegerField()
#     Total_price = models.BigIntegerField()
#
# class options6(models.Model):
#     per_sq_ft=models.BigIntegerField()
#     Total_price = models.BigIntegerField()
#
# class options7(models.Model):
#     per_sq_ft=models.BigIntegerField()
#     Total_price = models.BigIntegerField()

class apartment(models.Model):
    apartment_no = models.CharField(max_length=100, blank=False, unique=True)
    apt_flat_type = models.ForeignKey(Flat_type,blank=True,null=True)
    apt_foor_no = models.ForeignKey(Floor_no)
    apt_view_name = models.ForeignKey(apt_view)
    apt_building = models.ForeignKey(building)
    actual_area_suite = models.FloatField()
    actual_area_balcony = models.FloatField()
    is_available = models.NullBooleanField(default=True)

    # def _get_total_area(self):
    #     "Returns the total"
    #     return self.actual_area_suite + self.actual_area_balcony
    #
    #
    # total_area_sqft = property(_get_total_area)
    #
    cost_per_sqft = models.BigIntegerField()
    #total_area = models.IntegerField(blank=True,null=True)
    #total_price = models.IntegerField(blank=True,null=True)





    #
    # def _get1_price(self):
    #     "Returns the total"
    #     one= self.cost_per_sqft
    #
    #     return one * self.total_area_sqft
    #
    # option1_total_price = property(_get1_price)
    #
    # def _get2_price(self):
    #     "Returns the total"
    #     one= self.cost_per_sqft + int(35)
    #
    #     return one * self.total_area_sqft
    #
    # option2_total_price = property(_get2_price)
    #
    #
    # def _get3_price(self):
    #     "Returns the total"
    #     two = self.cost_per_sqft + int(40)
    #
    #     return two * self.total_area_sqft
    #
    # option3_total_price = property(_get3_price)
    #
    # def _get4_price(self):
    #     "Returns the total"
    #     one = self.cost_per_sqft + int(55)
    #
    #     return one * self.total_area_sqft
    #
    # option4_total_price = property(_get4_price)
    #
    # def _get5_price(self):
    #     "Returns the total"
    #     one = self.cost_per_sqft + int(70)
    #
    #     return one * self.total_area_sqft
    #
    # option5_total_price = property(_get5_price)
    #
    # def _get6_price(self):
    #     "Returns the total"
    #     one = self.cost_per_sqft + int(80)
    #
    #     return one * self.total_area_sqft
    #
    # option6_total_price = property(_get6_price)
    #
    # def _get7_price(self):
    #     "Returns the total"
    #     one = self.cost_per_sqft + int(100)
    #
    #     return one * self.total_area_sqft
    #
    # option7_total_price = property(_get7_price)


    #Option_selected = models.ForeignKey(options)
    # Options3 = models.ForeignKey(options3)
    # Options4 = models.ForeignKey(options4)
    # Options5 = models.ForeignKey(options5)
    # Options6 = models.ForeignKey(options6)
    # Options7 = models.ForeignKey(options7)
    # date_created = models.DateTimeField(auto_now_add=True)
    # date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.apartment_no)


class Userdata(models.Model):
    user_name = models.CharField(max_length=100)
    user_email = models.EmailField(null=True,blank=True)
    user_mobile = models.IntegerField(null=True,blank=True)
    #intrested_in_apt = models.ForeignKey(apartment)
    payment_plan = models.NullBooleanField(default=True,null=True,blank=True)
    post_handover_plan = models.NullBooleanField(default=True,null=True,blank=True)
    years_of_pay = models.IntegerField(null=True,blank=True)
    percentage_of_pay = models.IntegerField(null=True,blank=True)
    unit_size = models.CharField(max_length=20,null=True,blank=True)
    unit_type = models.CharField(max_length=20,null=True,blank=True)
    apt_name = models.ForeignKey(apartment,null=True,blank=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.user_name)

class test_conflict_model(models.Model):
    pass





