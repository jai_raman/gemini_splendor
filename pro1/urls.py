from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.routers import DefaultRouter
router = DefaultRouter()
urlpatterns = router.urls
from app1 import views

urlpatterns = urlpatterns+[
    url(r'^admin/', include(admin.site.urls)),
    url(r'',include('app1.urls')),
]
